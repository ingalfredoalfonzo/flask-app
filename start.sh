#!/bin/bash
app="flask-app-swagger"
docker build -t ${app} .
docker run -d -p 56785:80 \
  --name=${app} \
  -v $PWD:/app ${app}