from flask_restplus import Api, Resource
from app import app

api = Api(app)
name_space = api.namespace('test', description='Test APIs')
ns_other = api.namespace('other', description='Other APIs')

@name_space.route("/test")
class MainClass(Resource):
	def get(self):
		return {
			"status": "Got new data"
	    }
	def post(self):
		return {
			"status": "Posted new data"
		}

@ns_other.route("/docs")
class MainClass(Resource):
	def get(self):
		return {
			"status": "Got new data"
	    }
	def post(self):
		return {
			"status": "Posted new data"
		}